<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Ehros
 * @since 0.0.1
 */
get_header(); ?>
	<main id="main-content" class="site-main" role="main">
		<section class="home-about">

			<div class="gallery">
				<figure class="first-image ">
					<img class="shadow" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ehros-about-image-1.jpg" alt="Lorem ipsum dolor sit amet">
					<figcaption>Lorem ipsum dolor sit amet</figcaption>
				</figure>

				<figure class="second-image">
					<img class="shadow" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ehros-about-image-2.jpg" alt="Lorem ipsum dolor sit amet">
					<figcaption>Lorem ipsum dolor sit amet</figcaption>
				</figure>
			</div><!-- /.gallery -->

			<aside>
				<h2 class="pacifico">Quem é Ehros?</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis sollicitudin lectus. Praesent non lorem risus. Curabitur tellus nibh, consectetur non pellentesque et, pulvinar id diam. Quisque fermentum, turpis ac suscipit pretium, arcu diam tristique tellus, sed vestibulum felis massa non nibh</p>
			</aside>

		</section><!-- /.home-about -->

		<div class="clear"></div>

		<!-- Start banner bottom -->
		<div class="row banner-bottom align-items-center justify-content-center">
			<div class="col-lg-4">
				<div class="video-popup d-flex align-items-center">
					<a class="play-video video-play-button animate" href="https://www.youtube.com/watch?v=???????" data-animate="zoomIn"
					 data-duration="1.5s" data-delay="0.1s">
						<span><img src="<?php echo get_template_directory_uri(); ?>/assets/images/play-icon.png" alt=""></span>
					</a>
					<div class="watch">
						<h6>Watch video</h6>
						<p>You will love our execution</p>
					</div>
				</div>
			</div>
			<div class="col-lg-8">
				<div class="banner_content">
					<div class="row d-flex align-items-center">
						<div class="col-lg-8 col-md-12">
							<p class="top-text">Ehros Gourmet</p>
							<h1>Ehros Gourmet offers best candy in town</h1>
							<p>inappropriate is often laughed off as “boys will be,” face higher standards
								especially in place. That’s why it’s crucial that, as.</p>
						</div>
						<div class="col-lg-4 col-md-12">
							<div class="banner-btn">
								<a class="primary-btn text-uppercase" href="#">Explore Menu</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main><!-- #content -->
<?php
get_footer();