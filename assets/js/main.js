
// Define a altura do Header
let $screen = document.getElementById('header');

if ( window.innerHeight >= 300 ) {
	$screen.style.height = window.innerHeight - 100 + 'px';
} else {
	$screen.style.height = '300px';
}

window.addEventListener('resize', function(){
	if ( window.innerHeight >= 300 ) {
		$screen.style.height = window.innerHeight - 100 + 'px';
	}
}, true);