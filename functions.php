<?php

function ehros_setup_theme() {

	// Generate Title Tag
	add_theme_support( 'title-tag' );

	// Add Thumbnails support 
	add_theme_support( 'post-thumbnails' );

	// Add Custom Logo support
	add_theme_support( 'custom-logo' );

	// Add Custom Header support
	$args = array(
		'width'         => 2000,
		'height'        => 1200,
		'default-image' => get_template_directory_uri() . '/assets/images/ehros-frontpage-background.jpg',
		'uploads'       => true,
	);
	add_theme_support( 'custom-header', $args );

}
add_action( 'after_setup_theme', 'ehros_setup_theme' );

/**
 * Load site scripts.
 *
 * @since  0.0.1
 *
 * @return void
 */
function ehros_enqueue_scripts() {

	$template_url = get_template_directory_uri();

	wp_enqueue_style( 'pacifico', 'https://fonts.googleapis.com/css?family=Pacifico', array(), null, 'all' );
	wp_enqueue_style( 'quicksand', 'https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700', array(), null, 'all' );
	
	wp_enqueue_style( 'bootstrap-style', $template_url . '/assets/css/bootstrap.css', array(), null, 'all' );
	wp_enqueue_style( 'ehros-style', $template_url . '/assets/css/style.css', array(), null, 'all' );

	// jQuery.
	//wp_enqueue_script( 'jquery' );

	wp_enqueue_script( 'main', $template_url . '/assets/js/main.js', array(), null, 'all' );

}
add_action( 'wp_enqueue_scripts', 'ehros_enqueue_scripts', 1 );