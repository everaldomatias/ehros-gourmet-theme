<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till #main div
 *
 * @package Ehros
 * @since 0.0.1
 */
?><!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php if ( ! get_option( 'site_icon' ) ) : ?>
		<link href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico" rel="shortcut icon" />
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header id="header" role="banner" style="background-image: url('<?php echo header_image(); ?>')">
		<section class="intro">
			<div class="inner">
				<h1 class="pacifico"><?php bloginfo( 'name' ); ?></h1>
				<p><?php bloginfo( 'description' ); ?></p>
			</div><!-- /.inner -->
		</section><!-- /.intro -->
	</header><!-- #header -->

	<div class="container wrapper">
		<div class="row">